<?php
 /**
 * Filename: UsersTableSeeder.php
 * Last modified: 2/2/20, 5:18 PM
 *
 * @author Panji Setya Nur Prawira
 * @facebook https://www.facebook.com/PanjiNamjaElf
 * @twitter https://twitter.com/PanjiNamjaElf18
 * @github https://github.com/PanjiNamjaElf/
 * @gitlab https://gitlab.com/PanjiNamjaElf
 * @copyright (c) 2020
 */

 use Illuminate\Database\Seeder;

 class UsersTableSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
   DB::table('users')->truncate();
   DB::table('users')->insert([
    'name'       => 'Panji Setya Nur Prawira',
    'username'   => 'PanjiNamjaElf',
    'password'   => bcrypt('nezuko'),
    'created_at' => \Carbon\Carbon::now(),
    'updated_at' => \Carbon\Carbon::now(),
   ]);
  }
 }
